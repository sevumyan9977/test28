<?php

namespace App\Repository\Read\Car;

use App\Exceptions\Car\CarDoesNotExistException;
use App\Models\Car;
use Illuminate\Database\Eloquent\Builder;

class CarReadRepository implements CarReadRepositoryInterface
{
    private function query(): Builder
    {
       return Car::query();
    }

    /**
     * @throws CarDoesNotExistException
     */
    public function getById(int $id, array $relations = []): Car
    {
        /* @var Car $car */
        if (!$car = $this->query()->where('id', $id)->with($relations)->first()) {
            throw new CarDoesNotExistException();
        }

        return $car;
    }
}
