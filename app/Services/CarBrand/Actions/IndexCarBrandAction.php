<?php

namespace App\Services\CarBrand\Actions;

use App\Services\CarBrand\Dto\IndexCarBrandDto;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Repository\Read\CarBrand\CarBrandReadRepositoryInterface;

class IndexCarBrandAction
{
    public function __construct(
        private readonly CarBrandReadRepositoryInterface $carBrandReadRepository
    ) {
    }

    public function run(IndexCarBrandDto $dto): LengthAwarePaginator
    {
        return $this->carBrandReadRepository->index($dto);
    }
}
