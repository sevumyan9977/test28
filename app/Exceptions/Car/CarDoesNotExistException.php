<?php

namespace App\Exceptions\Car;

use App\Exceptions\BusinessLogicException;

class CarDoesNotExistException extends BusinessLogicException
{

    public function getStatus(): int
    {
        return BusinessLogicException::CAR_DOES_NOT_EXIST;
    }

    public function getStatusMessage(): string
    {
        return 'Car does not exist';
    }
}
