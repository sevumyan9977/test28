<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('model_id');
            $table->unsignedBigInteger('brand_id');
            $table->integer('year');
            $table->integer('mileage');
            $table->string('color');
            $table->timestamps();

            $table->foreign('model_id')
                ->references('id')
                ->on('car_models')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->foreign('brand_id')
                ->references('id')
                ->on('car_brands')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cars');
    }
};
