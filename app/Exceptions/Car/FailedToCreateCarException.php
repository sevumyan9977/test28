<?php

namespace App\Exceptions\Car;

use App\Exceptions\BusinessLogicException;

class FailedToCreateCarException extends BusinessLogicException
{

    public function getStatus(): int
    {
        return BusinessLogicException::FAILED_TO_CREATE_CAR;
    }

    public function getStatusMessage(): string
    {
        return 'Failed to create car';
    }
}
