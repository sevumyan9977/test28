<?php

namespace App\Http\Resources\Car;

use Illuminate\Http\Request;
use App\Http\Resources\CarModel\CarModelResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'year' => $this->resource->year,
            'mileage' => $this->resource->mileage,
            'color' => $this->resource->color,
            'model' => new CarModelResource($this->whenLoaded('model')),
            'brand' => new CarModelResource($this->whenLoaded('brand'))
        ];
    }

}
