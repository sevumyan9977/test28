<?php

namespace App\Exceptions\Car;

use App\Exceptions\BusinessLogicException;

class FailedToDeleteCarException extends BusinessLogicException
{

    public function getStatus(): int
    {
        return BusinessLogicException::FAILED_TO_DELETE_CAR;
    }

    public function getStatusMessage(): string
    {
        return 'Failed to delete car';
    }
}
