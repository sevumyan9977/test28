<?php

namespace App\Repository\Read\CarModel;

use App\Models\CarModel;
use Illuminate\Database\Eloquent\Builder;
use App\Services\CarModel\Dto\IndexCarModelDto;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CarModelReadRepository implements CarModelReadRepositoryInterface
{
    private function query(): Builder
    {
        return CarModel::query();
    }

    public function index(IndexCarModelDto $dto, array $relations = []): LengthAwarePaginator
    {
        $query = $this->query();

        if ($dto->q) {
            $query->where('name', 'like', '%' . $dto->q . '%');
        }
        $query->with($relations);

        return $query->paginate(
            $dto->pagination->perPage,
            ['*'],
            'page',
            $dto->pagination->page
        );
    }
}
