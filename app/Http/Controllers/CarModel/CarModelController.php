<?php

namespace App\Http\Controllers\CarModel;

use App\Http\Controllers\Controller;
use App\Services\CarModel\Dto\IndexCarModelDto;
use App\Http\Resources\CarModel\CarModelResource;
use App\Http\Requests\CarModel\IndexCarModelRequest;
use App\Services\CarModel\Actions\IndexCarModelAction;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CarModelController extends Controller
{
    public function index(
        IndexCarModelRequest $request,
        IndexCarModelAction $indexCarModelAction
    ): AnonymousResourceCollection {
        $dto = IndexCarModelDto::fromRequest($request);
        $carModel = $indexCarModelAction->run($dto);

        return CarModelResource::collection($carModel);
    }
}
