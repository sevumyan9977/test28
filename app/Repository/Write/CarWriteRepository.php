<?php

namespace App\Repository\Write;

use App\Models\Car;
use Illuminate\Database\Eloquent\Builder;
use App\Exceptions\Car\FailedToCreateCarException;
use App\Exceptions\Car\FailedToDeleteCarException;
use App\Exceptions\Car\FailedToUpdateCarException;

class CarWriteRepository implements CarWriteRepositoryInterface
{
    private function query(): Builder
    {
        return Car::query();
    }

    /**
     * @throws FailedToCreateCarException
     */
    public function create(array $data): Car
    {
        /* @var Car $car */
        if (!$car = $this->query()->create($data)) {
            throw new FailedToCreateCarException();
        }

        return $car;
    }

    /**
     * @throws FailedToUpdateCarException
     */
    public function update(int $id, array $data): int
    {
        if (!$car = $this->query()->where('id', $id)->update($data)) {
            throw new FailedToUpdateCarException();
        }

        return $car;
    }

    /**
     * @throws FailedToDeleteCarException
     */
    public function delete(int $id): bool
    {
        if (!$this->query()->where('id', $id)->delete()) {
            throw new FailedToDeleteCarException();
        }

        return true;
    }
}
