<?php

namespace App\Services\Car\Actions;

use App\Exceptions\Car\FailedToCreateCarException;
use App\Models\Car;
use App\Services\Car\Dto\CreateCarDto;
use App\Repository\Write\CarWriteRepositoryInterface;

class CreateCarAction
{
    public function __construct(
        private readonly CarWriteRepositoryInterface $carWriteRepository
    ) {
    }

    /**
     * @throws FailedToCreateCarException
     */
    public function run(CreateCarDto $dto): Car
    {
        return $this->carWriteRepository->create(Car::createModel($dto));
    }
}
