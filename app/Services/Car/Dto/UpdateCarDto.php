<?php

namespace App\Services\Car\Dto;

use App\Http\Requests\Car\UpdateCarRequest;
use Spatie\LaravelData\Data;

class UpdateCarDto extends Data
{
    public int $id;
    public int $modelId;
    public int $brandId;
    public int $year;
    public int $mileage;
    public string $color;

    public static function fromRequest(UpdateCarRequest $request): self
    {
        return self::from([
            'id' => $request->getId(),
            'modelId' => $request->getModelId(),
            'brandId' => $request->getBrandId(),
            'year' => $request->getYear(),
            'mileage' => $request->getMileage(),
            'color' => $request->getColor()
        ]);
    }
}
