<?php

namespace App\Services\Car\Dto;

use App\Http\Requests\Car\CreateCarRequest;
use Spatie\LaravelData\Data;

class CreateCarDto extends Data
{
    public int $modelId;
    public int $brandId;
    public int $year;
    public int $mileage;
    public string $color;

    public static function fromRequest(CreateCarRequest $request): self
    {
        return self::from([
            'modelId' => $request->getModelId(),
            'brandId' => $request->getBrandId(),
            'year' => $request->getYear(),
            'mileage' => $request->getMileage(),
            'color' => $request->getColor()
        ]);
    }
}
