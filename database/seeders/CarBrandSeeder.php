<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\CarBrand as CarBrand;

class CarBrandSeeder extends Seeder
{
    public function run(): void
    {
        $query = CarBrand::query();

        $data = [
            [
                'name' => 'Mercedes',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'BMW',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Opel',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        $query->insert($data);
    }
}
