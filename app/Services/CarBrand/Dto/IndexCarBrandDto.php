<?php

namespace App\Services\CarBrand\Dto;

use Spatie\LaravelData\Data;
use App\Services\Dto\PaginationParamsDto;
use App\Http\Requests\CarBrand\IndexCarBrandRequest;

class IndexCarBrandDto extends Data
{
    public ?string $q;
    public PaginationParamsDto $pagination;

    public static function fromRequest(IndexCarBrandRequest $request): self
    {
        return self::from([
            'q' => $request->getQ(),
            'pagination' => PaginationParamsDto::from([
                'page'    => $request->getPage(),
                'perPage' => $request->getPerPage(),
            ]),
        ]);
    }
}
