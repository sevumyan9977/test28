<?php

namespace App\Http\Requests\Car;

use Illuminate\Foundation\Http\FormRequest;

class CreateCarRequest extends FormRequest
{
    private const MODEL_ID = 'model_id';
    private const BRAND_ID = 'brand_id';
    private const YEAR = 'year';
    private const MILEAGE = 'mileage';
    private const COLOR = 'color';

    public function rules(): array
    {
        return [
            self::MODEL_ID => [
                'integer',
                'required',
                'exists:car_models,id'
            ],
            self::BRAND_ID => [
                'integer',
                'required',
                'exists:car_brands,id'
            ],
            self::YEAR => [
                'integer',
                'required'
            ],
            self::MILEAGE => [
                'integer',
                'required'
            ],
            self::COLOR => [
                'string',
                'required'
            ]
        ];
    }

    public function getModelId(): int
    {
        return $this->get(self::MODEL_ID);
    }

    public function getBrandId(): int
    {
        return $this->get(self::BRAND_ID);
    }

    public function getYear(): int
    {
        return $this->get(self::YEAR);
    }

    public function getMileage(): int
    {
        return $this->get(self::MILEAGE);
    }

    public function getColor(): string
    {
        return $this->get(self::COLOR);
    }
}
