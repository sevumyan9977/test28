<?php

namespace App\Services\CarModel\Dto;

use Spatie\LaravelData\Data;
use App\Services\Dto\PaginationParamsDto;
use App\Http\Requests\CarModel\IndexCarModelRequest;

class IndexCarModelDto extends Data
{
    public ?string $q;
    public PaginationParamsDto $pagination;

    public static function fromRequest(IndexCarModelRequest $request): self
    {
        return self::from([
            'q' => $request->getQ(),
            'pagination' => PaginationParamsDto::from([
                'page' => $request->getPage(),
                'perPage' => $request->getPerPage(),
            ]),
        ]);
    }
}
