<?php

namespace App\Services\CarModel\Actions;

use App\Services\CarModel\Dto\IndexCarModelDto;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Repository\Read\CarModel\CarModelReadRepositoryInterface;

class IndexCarModelAction
{
    public function __construct(
        private readonly CarModelReadRepositoryInterface $carModelReadRepository
    ) {
    }

    public function run(IndexCarModelDto $dto): LengthAwarePaginator
    {
        return $this->carModelReadRepository->index($dto);
    }
}
