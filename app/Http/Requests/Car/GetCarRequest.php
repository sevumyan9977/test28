<?php

namespace App\Http\Requests\Car;

use Illuminate\Foundation\Http\FormRequest;

class GetCarRequest extends FormRequest
{
    private const ID = 'id';

    public function getId(): int
    {
        return $this->route(self::ID);
    }
}
