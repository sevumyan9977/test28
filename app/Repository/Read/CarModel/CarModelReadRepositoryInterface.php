<?php

namespace App\Repository\Read\CarModel;

use App\Services\CarModel\Dto\IndexCarModelDto;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface CarModelReadRepositoryInterface
{
    public function index(IndexCarModelDto $dto, array $relations = []): LengthAwarePaginator;
}
