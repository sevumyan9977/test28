<?php

namespace App\Services\Car\Actions;

use App\Exceptions\Car\FailedToDeleteCarException;
use App\Repository\Write\CarWriteRepositoryInterface;

class DeleteCarAction
{
    public function __construct(
        private readonly CarWriteRepositoryInterface $carWriteRepository
    ) {
    }

    /**
     * @throws FailedToDeleteCarException
     */
    public function run(int $id): void
    {
        $this->carWriteRepository->delete($id);
    }
}
