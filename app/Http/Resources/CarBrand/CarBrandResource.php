<?php

namespace App\Http\Resources\CarBrand;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarBrandResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'brand' => $this->resource->name
        ];
    }
}
