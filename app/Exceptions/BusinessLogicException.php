<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

abstract class BusinessLogicException extends Exception
{
    public const FAILED_TO_CREATE_CAR = 600;
    public const FAILED_TO_UPDATE_CAR = 601;
    public const CAR_DOES_NOT_EXIST = 602;
    public const FAILED_TO_DELETE_CAR = 603;

    private int $httpStatusCode = ResponseAlias::HTTP_BAD_REQUEST;

    public function getHttpStatusCode(): int
    {
        return $this->httpStatusCode;
    }

    abstract public function getStatus(): int;

    abstract public function getStatusMessage(): string;
}
