<?php

namespace App\Providers;

use Carbon\Laravel\ServiceProvider;
use App\Repository\Write\CarWriteRepository;
use App\Repository\Read\Car\CarReadRepository;
use App\Repository\Write\CarWriteRepositoryInterface;
use App\Repository\Read\Car\CarReadRepositoryInterface;
use App\Repository\Read\CarBrand\CarBrandReadRepository;
use App\Repository\Read\CarModel\CarModelReadRepository;
use App\Repository\Read\CarModel\CarModelReadRepositoryInterface;
use App\Repository\Read\CarBrand\CarBrandReadRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(
            CarBrandReadRepositoryInterface::class,
            CarBrandReadRepository::class
        );

        $this->app->bind(
            CarModelReadRepositoryInterface::class,
            CarModelReadRepository::class
        );

        $this->app->bind(
            CarWriteRepositoryInterface::class,
            CarWriteRepository::class
        );

        $this->app->bind(
            CarReadRepositoryInterface::class,
            CarReadRepository::class
        );
    }
}
