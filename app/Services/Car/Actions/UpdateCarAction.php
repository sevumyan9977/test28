<?php

namespace App\Services\Car\Actions;

use App\Exceptions\Car\FailedToUpdateCarException;
use App\Models\Car;
use App\Services\Car\Dto\UpdateCarDto;
use App\Repository\Write\CarWriteRepositoryInterface;

class UpdateCarAction
{
    public function __construct(
        private readonly CarWriteRepositoryInterface $carWriteRepository
    ) {
    }

    /**
     * @throws FailedToUpdateCarException
     */
    public function run(UpdateCarDto $dto): void
    {
        $this->carWriteRepository->update($dto->id, Car::updateModel($dto));
    }
}
