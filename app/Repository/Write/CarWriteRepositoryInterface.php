<?php

namespace App\Repository\Write;

use App\Exceptions\Car\FailedToCreateCarException;
use App\Exceptions\Car\FailedToDeleteCarException;
use App\Exceptions\Car\FailedToUpdateCarException;
use App\Models\Car;

interface CarWriteRepositoryInterface
{
    /**
     * @throws FailedToCreateCarException
     */
    public function create(array $data): Car;

    /**
     * @throws FailedToUpdateCarException
     */
    public function update(int $id, array $data): int;

    /**
     * @throws FailedToDeleteCarException
     */
    public function delete(int $id): bool;
}
