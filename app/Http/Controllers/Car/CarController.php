<?php

namespace App\Http\Controllers\Car;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Services\Car\Dto\UpdateCarDto;
use App\Services\Car\Dto\CreateCarDto;
use App\Http\Resources\Car\CarResource;
use App\Http\Requests\Car\GetCarRequest;
use App\Services\Car\Actions\GetCarAction;
use App\Http\Requests\Car\CreateCarRequest;
use App\Http\Requests\Car\DeleteCarRequest;
use App\Http\Requests\Car\UpdateCarRequest;
use App\Services\Car\Actions\CreateCarAction;
use App\Services\Car\Actions\DeleteCarAction;
use App\Services\Car\Actions\UpdateCarAction;
use App\Exceptions\Car\CarDoesNotExistException;
use App\Exceptions\Car\FailedToDeleteCarException;
use App\Exceptions\Car\FailedToUpdateCarException;
use App\Exceptions\Car\FailedToCreateCarException;

class CarController extends Controller
{
    /**
     * @throws FailedToCreateCarException
     */
    public function create(
        CreateCarRequest $request,
        CreateCarAction $createCarAction
    ): CarResource {
        $dto = CreateCarDto::fromRequest($request);
        $car = $createCarAction->run($dto);

        return new CarResource($car);
    }

    /**
     * @throws FailedToUpdateCarException
     */
    public function update(
       UpdateCarRequest $request,
       UpdateCarAction $updateCarAction
    ): JsonResponse {
        $dto = UpdateCarDto::fromRequest($request);
        $updateCarAction->run($dto);

        return $this->response();
    }

    /**
     * @throws CarDoesNotExistException
     */
    public function show(
        GetCarRequest $request,
        GetCarAction $getCarAction
    ): CarResource {
        $car = $getCarAction->run($request->getId());

        return new CarResource($car);
    }

    /**
     * @throws FailedToDeleteCarException
     */
    public function delete(
        DeleteCarRequest $request,
        DeleteCarAction $deleteCarAction
    ): JsonResponse {
        $deleteCarAction->run($request->getId());

        return $this->response();
    }
}
