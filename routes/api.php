<?php

use App\Http\Controllers\Car\CarController;
use App\Http\Controllers\CarModel\CarModelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarBrand\CarBrandController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('brands', [CarBrandController::class ,'index']);

Route::get('models', [CarModelController::class ,'index']);

Route::prefix('cars')->group(function (){
    Route::post('/', [CarController::class, 'create']);
    Route::patch('/', [CarController::class, 'update']);
    Route::get('/{id}', [CarController::class, 'show']);
    Route::delete('/{id}', [CarController::class, 'delete']);
});
