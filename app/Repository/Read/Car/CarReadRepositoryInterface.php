<?php

namespace App\Repository\Read\Car;

use App\Exceptions\Car\CarDoesNotExistException;
use App\Models\Car;

interface CarReadRepositoryInterface
{
    /**
     * @throws CarDoesNotExistException
     */
    public function getById(int $id, array $relations = []): Car;
}
