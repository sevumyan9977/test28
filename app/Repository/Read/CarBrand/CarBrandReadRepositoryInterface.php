<?php

namespace App\Repository\Read\CarBrand;

use App\Services\CarBrand\Dto\IndexCarBrandDto;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface CarBrandReadRepositoryInterface
{
    public function index(IndexCarBrandDto $dto, array $relations = []): LengthAwarePaginator;
}
