<?php

namespace App\Services\Dto;

use Spatie\LaravelData\Data;

class PaginationParamsDto extends Data
{
    public int $perPage;
    public int $page;
}
