<?php

namespace App\Repository\Read\CarBrand;

use App\Models\CarBrand;
use Illuminate\Database\Eloquent\Builder;
use App\Services\CarBrand\Dto\IndexCarBrandDto;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CarBrandReadRepository implements CarBrandReadRepositoryInterface
{
    private function query(): Builder
    {
        return CarBrand::query();
    }

    public function index(IndexCarBrandDto $dto, array $relations = []): LengthAwarePaginator
    {
        $query = $this->query();

        if ($dto->q) {
            $query->where('name', 'like', '%' . $dto->q . '%');
        }

        return $query->with($relations)->paginate(
            $dto->pagination->perPage,
            ['*'],
            'page',
            $dto->pagination->page
        );

    }
}
