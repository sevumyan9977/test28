<?php

namespace App\Exceptions\Car;

use App\Exceptions\BusinessLogicException;

class FailedToUpdateCarException extends BusinessLogicException
{

    public function getStatus(): int
    {
        return BusinessLogicException::FAILED_TO_UPDATE_CAR;
    }

    public function getStatusMessage(): string
    {
        return 'Failed to update car';
    }
}
