<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\CarModel;
use App\Models\CarBrand;
use Illuminate\Database\Seeder;

class CarModelSeeder extends Seeder
{
    public function run(): void
    {
        $brand = CarBrand::query()->where('name', '=', 'BMW')->first();
        $query = CarModel::query();

        $data = [
            [
                'brand_id' => $brand->id,
                'name' => 'X5',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],

            [
                'brand_id' => $brand->id,
                'name' => 'X6',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],

            [
                'brand_id' => $brand->id,
                'name' => 'X7',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],

        ];

        $query->insert($data);
    }
}
