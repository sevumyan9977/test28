<?php

namespace App\Models;

use App\Services\Car\Dto\CreateCarDto;
use App\Services\Car\Dto\UpdateCarDto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Car extends Model
{
    protected $table = 'cars';

    protected $fillable = [
      'id',
      'model_id',
      'brand_id',
      'year',
      'mileage',
      'color'
    ];

    public static function createModel(CreateCarDto $dto): array
    {
        return [
            'model_id' => $dto->modelId,
            'brand_id' => $dto->brandId,
            'year' => $dto->year,
            'mileage' => $dto->mileage,
            'color' => $dto->color
        ];
    }

    public static function updateModel(UpdateCarDto $dto): array
    {
        return [
            'model_id' => $dto->modelId,
            'brand_id' => $dto->brandId,
            'year' => $dto->year,
            'mileage' => $dto->mileage,
            'color' => $dto->color
        ];
    }

    public function model(): HasOne
    {
        return $this->hasOne(CarModel::class, 'id', 'model_id');
    }

    public function brand(): HasOne
    {
        return $this->hasOne(CarBrand::class, 'id', 'brand_id');
    }
}
