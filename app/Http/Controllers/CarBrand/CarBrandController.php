<?php

namespace App\Http\Controllers\CarBrand;

use App\Http\Controllers\Controller;
use App\Services\CarBrand\Dto\IndexCarBrandDto;
use App\Http\Resources\CarBrand\CarBrandResource;
use App\Http\Requests\CarBrand\IndexCarBrandRequest;
use App\Services\CarBrand\Actions\IndexCarBrandAction;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CarBrandController extends Controller
{
    public function index(
        IndexCarBrandRequest $request,
        IndexCarBrandAction $indexCarBrandAction
    ): AnonymousResourceCollection {
        $dto = IndexCarBrandDto::fromRequest($request);
        $carBrand = $indexCarBrandAction->run($dto);

        return  CarBrandResource::collection($carBrand);
    }
}
