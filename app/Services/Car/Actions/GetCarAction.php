<?php

namespace App\Services\Car\Actions;

use App\Exceptions\Car\CarDoesNotExistException;
use App\Models\Car;
use App\Repository\Read\Car\CarReadRepositoryInterface;

class GetCarAction
{
    public function __construct(
        private readonly CarReadRepositoryInterface $carReadRepository
    ) {
    }

    /**
     * @throws CarDoesNotExistException
     */
    public function run(int $id): Car
    {
        return $this->carReadRepository->getById($id, ['model', 'brand']);
    }
}
